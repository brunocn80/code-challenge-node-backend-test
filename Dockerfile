FROM node:12.18.1

COPY ./ ./

#RUN npm install --verbose
#RUN npm install --no-save --production --verbose

CMD ["node", "app.js"]
