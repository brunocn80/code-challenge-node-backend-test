
beforeEach(() => {
  jest.resetModules();
});

test('should create a hash from a string and verify that the hash and string "match"', async () => {
  const argon2 = require('@phc/argon2');
  const spyArgonHash = jest.spyOn(argon2, 'hash');
  const spyArgonVerify = jest.spyOn(argon2, 'verify');
  const auth = jest.requireActual('../../service/auth');
  const password = "password";
  const hash = await auth.hash(password);
  const result = await auth.verify({password, hash});
  expect(spyArgonHash).toBeCalledWith(password, {"iterations": 2, "memory": 2048, "parallelism": 2, "saltLength": 16});
  expect(spyArgonVerify).toBeCalledWith(hash, password);
  expect(result).toBeTruthy();
});

test('should not match the password with the hash from another string', async () => {
  const auth = jest.requireActual('../../service/auth');
  const password = "password";
  const hash = await auth.hash(password);
  const result = await auth.verify({password:'another_password', hash});
  expect(result).toBeFalsy();
})