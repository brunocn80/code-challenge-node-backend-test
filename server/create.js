// Forked workers will run this code when found to not be
// the master of the cluster.

const cluster = require('cluster');
const http = require('http');
const env = require('good-env');
const prometheus = require('prom-client');
const { Tags, initGlobalTracer } = require('opentracing');
const jaeger = require('jaeger-client');
const helper = require('./helper');

const serverPort = env.getNumber('SERVER_PORT', 8000);
const requests = new prometheus.Histogram({
  name: 'requests',
  help: 'latency',
  labelNames: ['status_code', 'worker_id', 'path', 'method'],
  buckets: [0.001, 0.003, 0.005, 0.01, 0.05, 0.1, 0.5, 1, 3, 5, 7, 10],
});
const configTracer = {
  serviceName: 'vma-backend-test',
  sampler: { type: 'const', param: 1 }
};
const tracer = jaeger.initTracerFromEnv(configTracer);
initGlobalTracer(tracer);

// Initialize routes & their handlers (once)
const router = require('find-my-way')() // https://github.com/delvedor/router-benchmark
require('./routes').config(router);
prometheus.collectDefaultMetrics();

module.exports = http.createServer((req, res) => {
  const rootSpan = tracer.startSpan(`${req.method} - ${req.url}`, {
    tags: {
      [Tags.SPAN_KIND]: Tags.SPAN_KIND_RPC_SERVER, [Tags.HTTP_METHOD]: req.method, [Tags.HTTP_URL]: req.url,
      ['worker_id']: `worker_${cluster.worker.id}`}
  });
  req.jaegerCtx = rootSpan.context();
  const stopTimer = requests.startTimer({'path': req.url, 'method': req.method});

  // request dispatched to handlers here
  router.lookup(req, res);

  stopTimer({worker_id: `worker_${cluster.worker.id}`, status_code: res.statusCode});
  rootSpan.addTags({[Tags.HTTP_STATUS_CODE] : res.statusCode});
  helper.finishSpan(rootSpan);

}).listen(serverPort, () => console.log(`NodeJS worker listening on port ${serverPort}`));
