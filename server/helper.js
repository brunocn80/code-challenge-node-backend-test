
const appJson = 'application/json';
const contType = 'content-type'
const { Tags, globalTracer } = require('opentracing');
const tracer = globalTracer();

const readBody = request => {
    const span = tracer.startSpan('reading request body', {
        childOf: request.jaegerCtx
    });
    return new Promise((resolve, reject) => {
        let body = [];
        request.on('data', (chunk) => {
            body.push(chunk);
        }).on('end', () => {
            body = Buffer.concat(body).toString()
            if (request.headers[contType] && (request.headers[contType].toLowerCase() === appJson)) {
                body = JSON.parse(body);
            }
            resolve(body);
            finishSpan(span);
        }).on('error', (err) => {
            console.error(err);
            reject(err);
            finishSpanWithError(span, err);
        });
    });
}

const handleError = ({e, res, status = 500, action=""}) => {
    console.error(`error ${action}`, e.message, e);
    res.statusCode = status;
    res.end(JSON.stringify({error : {message: e.message, data: e } }));
}

const sendResponse = ({status = 200, res, body}) => {
    res.statusCode = status;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(body));
}

const finishSpanWithError = (span, error) => {
    try {
        span.setTag(Tags.ERROR, true);
        span.log({ event: 'error', message: error.message, stack: error.stack });
        span.finish();
    } catch (err) {
        console.error('error finishing jaeger span', err);
    }
};

const finishSpan = span => {
    try {
        span.finish();
    } catch (err) {
        console.error('error finishing jaeger span', err);
    }
};

module.exports = { readBody, handleError, sendResponse, finishSpanWithError, finishSpan };