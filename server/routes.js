// Intialized database connections, one for each db config
// * Mongoose is a popular Node/MongoDB driver
// * Sequelize is a popular Node/SQL driver
const env = require('good-env');
const handler = require(`../handlers/${env.get('NODE_HANDLER', 'mongoose')}`);

module.exports.config = router => {
  router.on('POST', '/user', handler.createUser);
  router.on('DELETE', '/user/:id', handler.deleteUser);
  router.on('GET', '/user', handler.findUsers);
  router.on('PUT', '/user/:id', handler.updateUser);
  // router.on('POST', '/login', handler.authenticate);
};