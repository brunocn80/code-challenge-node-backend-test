db.auth('root', 'root');
db = db.getSiblingDB('vma_backend_test');
db.createUser(
    {
        user: "vma",
        pwd: "backend_test",
        roles: [
            {
                role: "readWrite",
                db: "vma_backend_test"
            }
        ]
    }
);
db.user.createIndex({ email: 1 }, { unique: true });