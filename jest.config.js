module.exports = {
  collectCoverage: true,
  verbose: true,
  collectCoverageFrom: [
    'app.js',
    'handlers/**/**.js',
    'server/**/**.js',
    'service/**/**.js',
  ],
  coverageDirectory: 'coverage/javascript/lcov',
  testEnvironment: 'node',
};
