The project was conceived thinking to run a benchmark in the end, against the 4 versions : mysql-raw, mysql-sequelize,mongo-raw. mongoose. To see who is faster and present the results, the metrics of those 4 persistence alternatives through dashboards. Mesuring where the bottlenecks are, with jaeger's distribuited tracing.

#### To run in your machine don't forget to change these properties in your `docker-compose.yml` :

`ADMINER_DEFAULT_SERVER: 192.168.1.5   # your docker0 network interface`

`ME_CONFIG_MONGODB_SERVER: 192.168.1.5   # your docker0 network interface`

#### Command to run the db migrations for mysql versions

```./node_modules/.bin/db-migrate up```

wrk -t8 -c8 -d10s -s create.lua -R10000 http://127.0.0.1:8000/user


