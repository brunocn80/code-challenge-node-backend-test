 CREATE TABLE IF NOT EXISTS `vma_backend_test`.`user` (
   `id` INT NOT NULL AUTO_INCREMENT,
   `name` VARCHAR(128) NOT NULL,
   `birthdate` DATE NOT NULL,
   `email` VARCHAR(128) NOT NULL,
   `password` VARCHAR(128) NOT NULL,
   `address` INT NOT NULL,
   PRIMARY KEY (`id`),
   UNIQUE INDEX `email_UNIQUE` (`email` ASC),
   INDEX `idx_email` (`email` ASC),
   INDEX `fk_user_address_idx` (`address` ASC),
   CONSTRAINT `fk_user_address`
     FOREIGN KEY (`address`)
     REFERENCES `vma_backend_test`.`address` (`id`)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION);