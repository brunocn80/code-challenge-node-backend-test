CREATE TABLE IF NOT EXISTS `vma_backend_test`.`address` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `line1` VARCHAR(255) NOT NULL,
  `line2` VARCHAR(255) NULL,
  `town` VARCHAR(35) NOT NULL,
  `county` VARCHAR(35) NULL,
  `postcode` VARCHAR(8) NOT NULL,
  PRIMARY KEY (`id`));