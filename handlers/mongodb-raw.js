const { MongoClient, ObjectId } = require('mongodb');
const env = require('good-env');
const helper = require('../server/helper');

const collections = {
  user: null,
};
let db;

(async function() {
  try {
    let client = await MongoClient.connect(env.get('MONGO_URL', 'mongodb://localhost:27017/vma_backend_test?maxPoolSize=50'), {
      useNewUrlParser: true , useUnifiedTopology: true,
      auth : { user: env.get('MONGO_USER', 'vma'), password: env.get('MONGO_PASSWORD', 'backend_test') }
    });
    db = client.db("vma_backend_test");
    collections.user = db.collection('user');
  } catch(e) {
    console.error('error trying to connecto to mongodb', e);
  }
})()

module.exports = {

  createUser: async (req, res, params) => {
    try {
      body = await helper.readBody(req);
      result = await collections.user.insertOne(body);
      res.end(result);
    } catch (e) {

    }
  },

  deleteUser: async (req, res, params) => {
    collections.user.deleteOne( {"_id": ObjectId(params.id) }, result => {
      res.end(result);
    });
  },

  findUsers: async (req, res, params) => {
    res.end("not implemented");
  },

  updateUser: async (req, res, params) => {
    res.end("not implemented");
  }

};
