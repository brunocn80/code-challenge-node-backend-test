const h = require('../helper');
const env = require('good-env');

const Sequelize = require('sequelize');
const sequelize = new Sequelize('vma_backend_test', env.get('MYSQL_USER', 'root'), env.get('MYSQL_PASSWD', 'root'), {
  host: 'localhost',
  dialect: 'mysql',
  // logging: true
});

const Worlds = sequelize.define('World', {
  id: {
    type: 'Sequelize.INTEGER',
    primaryKey: true
  },
  randomNumber: { type: 'Sequelize.INTEGER' }
}, {
    timestamps: false,
    freezeTableName: true
  });

const Fortunes = sequelize.define('Fortune', {
  id: {
    type: 'Sequelize.INTEGER',
    primaryKey: true
  },
  message: { type: 'Sequelize.STRING' }
}, {
    timestamps: false,
    freezeTableName: true
  });

const randomWorldPromise = () => {
  return Worlds.findOne({
    where: { id: h.randomTfbNumber() }
  }).then((results) => {
    return results;
  }).catch((err) => {
    process.exit(1);
  });
};

module.exports = {

  createUser: async (req, res, params) => {
    res.end("not implemented");
  },

  deleteUser: async (req, res, params) => {
    res.end("not implemented");
  },

  findUsers: async (req, res, params) => {
    res.end("not implemented");
  },

  updateUser: async (req, res, params) => {
    res.end("not implemented");
  }

};
