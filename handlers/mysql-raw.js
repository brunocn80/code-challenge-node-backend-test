const h = require('../helper');
const mysql = require('mysql');
const env = require('good-env');

const connection = mysql.createConnection({
  host: env.get('MYSQL_URL', 'localhost'),
  user: env.get('MYSQL_USER', 'root'),
  password: env.get('MYSQL_PASSWORD', 'root'),
  database: env.get('MYSQL_DB', 'vma_backend_test'),
});

connection.connect();

const queries = {

};

module.exports = {

  createUser: async (req, res, params) => {
    res.end("not implemented");
  },

  deleteUser: async (req, res, params) => {
    res.end("not implemented");
  },

  findUsers: async (req, res, params) => {
    res.end("not implemented");
  },

  updateUser: async (req, res, params) => {
    res.end("not implemented");
  }

};
