const Mongoose = require('mongoose');
const env = require('good-env');
const helper = require('../server/helper');
const authService = require('../service/auth');
const mongoosePaginate = require('mongoose-paginate-v2');
const parseurl = require('parseurl'); // faster than native nodejs url package
const { globalTracer } = require('opentracing');
const tracer = globalTracer();

mongoosePaginate.paginate.options = {
  limit: env.getNumber('MONGOOSE_PAGE_SIZE', 5),
  customLabels: {
    totalDocs: 'totalUsers',
    docs: 'users',
  }
};

let connection;
let Users;

(async function() {
  try {
    connection = await Mongoose.connect(
        env.get('MONGO_URL', 'mongodb://localhost:27017/vma_backend_test?maxPoolSize=50'),
        {
          useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false,
          auth : { user: env.get('MONGO_USER', 'vma'), password: env.get('MONGO_PASSWORD', 'backend_test') }
        });
    Users = connection.model('User', UserSchema);
  } catch(e) {
    console.error(e)
  }
})()

// Mongoose Setup
const AddressSchema = new Mongoose.Schema({
  line1: String,
  line2: String,
  town: String,
  county: String,
  postcode: String,
});

const UserSchema = new Mongoose.Schema({
  name: String,
  birthday: Date,
  email: String,
  password: { type: String, select: false }, // Users.findOne({_id: id}).select('+password').exec(...);
  address: AddressSchema,
}, {
  collection: 'user'
});

UserSchema.plugin(mongoosePaginate);

const mongooseCreateUser = async (req, user) => {
  const span = tracer.startSpan('mongoose User.create', {
    childOf: req.jaegerCtx
  });
  try {
    let result = await Users.create(user);
    helper.finishSpan(span);
    return result;
  } catch (e) {
    helper.finishSpanWithError(span, e);
    throw e;
  }
}

module.exports = {

  createUser: async (req, res, params) => {
    try {
      let user =  await helper.readBody(req);
      user.password = await authService.hash(user.password);
      let result = await mongooseCreateUser(req, user);
      result.password = undefined;
      helper.sendResponse({status:201, res, body : result});
    } catch (e) {
      helper.handleError({e, res, action:"creating user"});
    }
  },

  deleteUser: async (req, res, params) => {
    try {
      let result = await Users.findByIdAndRemove(params.id);
      helper.sendResponse({status:200, res, body : result});
    } catch (e) {
      helper.handleError({e, res, action:"deleting user"});
    }
  },

  findUsers: async (req, res, params) => {
    try {
      let result = await Users.paginate({}, queryToPaginationOptions(req));
      helper.sendResponse({res, body : result});
    } catch (e) {
      helper.handleError({e, res, action:"finding users"});
    }
  },

  updateUser: async (req, res, params) => {
    try {
      let user =  await helper.readBody(req);
      await Users.findByIdAndUpdate(params.id, user);
      helper.sendResponse({status: 204, res});
    } catch (e) {
      helper.handleError({e, res, action:"updating users"});
    }
  }

};

const queryToPaginationOptions = (req) => {
  let options = {};
  const url = parseurl(req);
  const params = url.query.split("&");
  for (let param of params) {
    const kv = param.split("=");
    options[kv[0]] = kv[1];
  }
  if (options.select) {
    options.select = options.select.split(',').join(" ");
  }
  return options;
}
