const argon2 = require('@phc/argon2');

const options = {
    memory:      2048,
    iterations:  2,
    parallelism: 2,
    saltLength:  16,
}

const hash = async (pwd) => await argon2.hash(pwd, options);

const verify = async ({hash, password}) => await argon2.verify(hash, password);

module.exports = {
    hash,
    verify,
}