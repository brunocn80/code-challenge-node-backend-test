const cluster = require('cluster');
const http = require('http');
const env = require('good-env');
const numCPUs = require('os').cpus().length;

const AggregatorRegistry = require('prom-client').AggregatorRegistry;
const aggregatorRegistry = new AggregatorRegistry();
const metricsPort = env.getNumber('METRICS_SERVER_PORT', 8001);

if (cluster.isMaster) {
  // Fork workers.
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('exit', (worker, code, signal) => {
  	console.log([
  	  'A process exit was triggered, most likely due to a failed database action',
  	  'NodeJS test server shutting down now'].join('\n'));
    process.exit(1);
  });

  http.createServer((req, res) => {
    aggregatorRegistry.clusterMetrics((err, metrics) => {
      if (err) {
        console.error('error in cluster metrics, ', err.message, JSON.stringify(err));
      }
      res.setHeader('Content-Type', aggregatorRegistry.contentType);
      res.end(metrics);
    });
  }).listen(metricsPort, () => console.log(`NodeJS metrics server listening on port ${metricsPort}`));
} else {
  // Task for forked worker
  require('./server/create');
}
